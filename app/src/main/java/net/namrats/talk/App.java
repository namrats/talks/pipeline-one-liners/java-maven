package net.namrats.talk;

import net.namrats.talk.domain.Person;

public class App {
    public static void main(String[] args) {
        Person[] persons = {
           new Person("John", "Doe"),
           new Person("Jane", "Doe"),
           new Person("Dominique", "Doe"),
        };

        for (var person : persons) {
            System.out.println(person.toString());
        }
    }
}
