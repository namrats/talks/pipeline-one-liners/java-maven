package net.namrats.talk.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PersonTest {
    @Test
    public void peopleHaveNames() {
        var p = new Person("Jarjar", "Binks");

        assertEquals("Jarjar", p.firstName());
    }
}
